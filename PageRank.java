
public class PageRank {
	// transition matrix
	double[][] M;
	// graph size
	int N;
	// PageRank vector
	double[] v;
	
	public PageRank(int size) {
		this.N = size;
		this.M = new double[N][N];
		this.v = new double[N];
		for (int i=0; i<N; i++) {
			v[i] = (double) 1 / N;
		}
	}	
	// take graph adj. matrix as input
	public PageRank(int[][] matrix) {
		this.M = transitionMatrix(matrix);
		this.N = matrix.length;
		this.v = new double[N];
		for (int i=0; i<N; i++) {
			v[i] = (double) 1 / N;
		}		
	}
	// calculate Transition matrix with given graph adjacent matrix
	private double[][] transitionMatrix(int[][] matrix) {
		int n = matrix.length;
		double[][] T = new double[n][n];
		
		for (int i=0; i<n; i++) {
			int sum  = 0;
			for (int j=0; j<n; j++) {
				sum += matrix[i][j];
			}
			for (int j=0; j<n; j++) {
				if (sum != 0)
					T[j][i] = (double)matrix[i][j] / sum;
			}
		}
		return T;
	}
	public void printV(double[] V) {
		// print PageRank vector
		String format = " %.3f ";
		System.out.println();
		System.out.print("[ ");
		for(int i=0; i<N; i++) {
			System.out.printf(format, V[i]);
			if (i < N-1)
				System.out.print(", ");
		}
		System.out.print(" ]\n");
	}
	public void printM() {
		System.out.println();
		// print transition matrix
		String format = " %.2f ";
		for (int i=0; i<N; i++) {
			for (int j=0; j<N; j++) {
				System.out.printf(format, M[i][j]);
			}	
			System.out.print("\n");
		}
	}
	
	// v' = beta*M*v + (1-beta)e/N
	// V - Page Rank vector, v0 = [1/N...1/N]
	// M - transition matrix
	// real life beta values 0.8 - 0.9
	public double[] calculate(double beta, double[] V) {
		double[] Vprim = new double[this.N];
		for(int i=0; i<N; i++) {
			// xi = M * v
			double xi = 0d;
			for(int j=0; j<N; j++) {
				xi += M[i][j] * V[j]; 
			}
			Vprim[i] = beta * xi + (1-beta) * 1 / N;
		}
		return Vprim;
	}
}
