import java.util.Map.Entry;
import java.util.TreeMap;

public class A7 {

	public static void main(String[] args) {
		String file_name = "Part-3A-graph-1.txt";
		//String file_name = "text-graph.txt";
		
		SimpleGraph graph = new SimpleGraph(4);
		graph.loadDataRaw(file_name);
		
		graph.print();
		
		PageRank pg = new PageRank(graph.matrix);
		pg.printM();
		pg.printV(pg.v);
		
		// calculate iterations
		int iterations = 7;
		double[] v = pg.v;
		for (int i=0; i<iterations; i++) {
			System.out.println("");
			System.out.println("iteration: " + (i+1));
			v = pg.calculate(0.8, v);
			pg.printV(v);
		}
		/*
		// sort nodes by pg
		TreeMap<Double, Integer> tree = new TreeMap<>();
		for(int i=0; i<pg.N; i++) {
			tree.put(v[i], i+1);
		}
		for (Entry<Double, Integer> e:tree.entrySet()) {
			System.out.println("node: " + e.getValue() + " pg: " + e.getKey());
		}
		*/
	}

}
