import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class SimpleGraph {
	// Store graph as adjacent table
	//
	int[][] matrix;
	boolean IS_DIRECTED = false;
	
	public SimpleGraph (int size){
		matrix = new int[size][size];
	}
	public SimpleGraph (int size, boolean directed){
		matrix = new int[size][size];
		this.IS_DIRECTED = directed;
	}
	public void loadDataRaw(String file_name) {
		// get the data
		File f = new File(file_name);

		try (Scanner scanner = new Scanner(f)){	
			while(scanner.hasNextLine()) {
				String[] tokens = scanner.nextLine().split("\n");
				// read line by line
				for(String i:tokens) {
					// split line
					String[] line = i.split("\\t");	// get data by split by TAB
					// add data
					// nodes are 1-N
					int from = Integer.valueOf( line[0] ) - 1 ;
					int to = Integer.valueOf( line[1] ) - 1;
					// undirected !
					if( IS_DIRECTED)
						this.addEdgeDir(from, to);
					else
						this.addEdge(from, to);											
				}	
			}
		} catch (FileNotFoundException ex) {
			System.out.println(ex);
		}				
	
	}
	public void addEdgeDir(int a, int b) {
		matrix[a][b]++;
	}
	public void addEdgeUdir(int a, int b) {
		this.addEdgeDir(a, b);
		this.addEdgeDir(b, a);
	}
	public void addEdge(int a, int b) {
		this.addEdgeUdir(a, b);
	}
	public void print() {
		
		for (int i=0; i<this.matrix.length; i++) {
			for (int j=0; j<this.matrix[i].length; j++) {
				System.out.print(matrix[i][j]);
			}
			System.out.print("\n");
		}
	}
}
